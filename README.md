# README #

### How to run ###
Those are step required to run the script on clear BeagleBone Black/Green Debian distro

* Install python 3:

    root@ulc032:/opt/CANbootloader# apt-get install python3

* no module "can" so python3 pip must be installed in order to install can module:

    root@ulc032:/opt/CANbootloader# apt-get install python3-pip

* install can module (info on the webpage https://python-can.readthedocs.io/en/latest/):

    root@ulc032:/opt/CANbootloader# pip-3.2 install python-can

* install tqdm module (bars progress in console):

    root@ulc032:/opt/CANbootloader# pip-3.2 install tqdm

* load the firmware by switching resting the STM32 with properly configured BOOT lines (can be controlled by GPIOs)

    root@ulc032:/opt/CANbootloader# python3 CANbootloader.py -e -w -v /opt/CANbootloader/mySoftware.bin

### What is this repository for? ###

* Please watch the gif CAN_bootloader_video.gif for reference
* Version 1.0
* Marcin Gajewski, sendtopolish@gmail.com

* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact