#!/usr/bin/python3


import can
from bbb_gpio import SimpleGPIO
import time
import os
import sys
import argparse

try:
    from tqdm import tqdm, trange
    usebar = 1
except:
    usebar = 0
# these come from AN2606

chip_ids = {
    0x412: "STM32 Low-density",
    0x410: "STM32 Medium-density",
    0x414: "STM32 High-density",
    0x420: "STM32 Medium-density value line",
    0x428: "STM32 High-density value line",
    0x430: "STM32 XL-density",
    0x416: "STM32 Medium-density ultralow power line",
    0x411: "STM32F2xx",
    0x413: "STM32F4xx",
    0x419: "STM32F429",
}

commands_ids = {
    0x000: "Get",
    0x001: "Get Version & Read Protection Status",
    0x002: "Get ID",
    0x003: "Speed",
    0x011: "Read Memory",
    0x021: "Go",
    0x031: "Write Memory",
    0x043: "Erase",
    0x063: "Write Protect",
    0x073: "Write Unprotect",
    0x082: "Readout Protect",
    0x092: "Readout Unprotect",
}

speed_can = {
    "125k" : 0x01,
    "250k" : 0x02,
    "500k" : 0x03,
    "1M": 0x04,
}

class CmdException(Exception):
    pass


class CommandInterface:
    extended_erase = 1
    bus = 0
    msg = can.Message(extended_id=False)
    gpio_nrst = None
    gpio_boot = None

    def __init__(self, channel = "can1", interface = "socketcan" , nrst = None, boot= None):
        self.channel = channel
        self.interface = interface
        if nrst is not None:
            self.gpio_nrst = SimpleGPIO(nrst)
        if boot is not None:
            self.gpio_boot = SimpleGPIO(boot)

    def open(self):
        print("Opening CANbus inteface")
        self.bus = can.interface.Bus(channel=self.channel ,bustype=self.interface)
        self.bus.flush_tx_buffer()


    def _wait_for_ask(self, info="", timeout = None):
        # wait for ask
        try:
            self.msg = self.bus.recv(timeout)
        except:
            raise CmdException("Can't read port or timeout")
        else:
            if self.msg.data[0] == 0x79:
                # ACK
                # print("Manage to syncro with uP")
                return 1
            else:
                if self.msg.data[0] == 0x1F:
                    # NACK
                    raise CmdException("NACK " + info)
                else:
                    # Unknown responce
                    raise CmdException("Unknown response. " + info + ": " + hex(self.msg.data[0]))


    def reset(self):
        if self.gpio_nrst is not None:
            # self.sp.setDTR(0)
            print("NRST LOW")
            self.gpio_nrst.write(0)
            time.sleep(0.5)
            print("NRST HIGH")
            self.gpio_nrst.write(1)
            time.sleep(0.5)
        else:
            pass

    def initChip(self):
        # Set boot
        if self.gpio_boot is not None:
            print("BOOT HIGH")
            self.gpio_boot.write(1)
            time.sleep(0.5)
        else:
            pass

        self.reset()

        print("*** Initialize command\n\r    |\n\r    ", end="")
        # self.sp.write("\x7F")  # Syncro
        self.msg.arbitration_id = 0x079
        # self.msg.data = None
        self.msg.extended_id = False
        self.bus.send(self.msg)
        return self._wait_for_ask("Syncro")

    def releaseChip(self):
        if self.gpio_boot is not None:
            print("BOOT LOW")
            self.gpio_boot.write(0)
            time.sleep(0.5)
            self.reset()
        else:
            pass

    def cmdGetID(self):
        if self.cmdGeneric(0x02):
            print("*** Get ID command\n\r    |\n\r    ", end="")
            self.msg = self.bus.recv(0.1)
            id = self.msg.data[0] << 8 |self.msg.data[1]
            self._wait_for_ask("0x02 end")
            print("'->Chip id: 0x%x %s" % (id, chip_ids.get(id, "Unknown")))
            return id
        else:
            raise CmdException("GetID (0x02) failed")

    def cmdGeneric(self, cmd, data = None, ack = True):
        # self.msg = can.Message(extended_id=None)
        if data is None:
            self.msg.data = self.msg.data[8:]
        else:
            self.msg.data = data

        self.msg.arbitration_id = cmd
        self.bus.send(self.msg)
        if ack is True:
            return self._wait_for_ask(hex(cmd))
        else:
            return 1

    def cmdGet(self):
        print("*** Get command\n\r    |\n\r    ", end="")
        if self.cmdGeneric(0x00):
            self.msg = self.bus.recv(0.1)
            numberofbytes = self.msg.data[0]
            print("'->Number of bytes: "+ str(numberofbytes))

            self.msg = self.bus.recv(0.1)
            bootloaderversion = self.msg.data[0]
            print("    '->Bootloader version: "+ hex(bootloaderversion))

            print("         '->Available commands: ")
            for n in range (0, numberofbytes):
                self.msg = self.bus.recv(0.1)
                print("                 0x%x %s" %(self.msg.data[0], commands_ids.get(self.msg.data[0], "Unknown")))

            self._wait_for_ask("Get command")
            return bootloaderversion
        else:
            raise CmdException("Get (0x00) failed")


    def cmdSpeed(self, port = "can1", speed = "125k"):
        print("*** Speed command\n\r    |\n\r    ", end="")
        candata = bytearray()
        candata.append(speed_can[speed])
        if self.cmdGeneric(0x03, candata):
            print("'->New CANbus speed set to %sbps" % speed)
            self.setCANspeed(port, speed)
            self.msg.arbitration_id = 0x079
            # self.msg.data = None
            self.msg.extended_id = False
            self.bus.send(self.msg)
            # return self._wait_for_ask("Syncro")
            return 1
        else:
            raise CmdException("Speed command (0x03) failed")

    def setCANspeed(self, port = "can1", newspeed = "125k"):
        os.system("cp /etc/network/interfaces.d/can1.%s /etc/network/interfaces.d/can1.cfg" %newspeed)
        os.system("ifdown %s && ifup %s" % (port, port))


    def cmdEraseMemory(self, sectors=None):
        if self.extended_erase:
            return cmd.cmdExtendedEraseMemory()

    def cmdExtendedEraseMemory(self):
        print("*** Extended Erase memory command\n\r    |\n\r    ", end="")
        eraseCom = [0xFF]
        if self.cmdGeneric(0x43, eraseCom):
            print("'->Extended erase in progress. This can take ten seconds or more...")
            if self._wait_for_ask("0x43 erasing failed") is 1:
                print("    '->Extended erase successful!!!")

        else:
            raise CmdException("Extended Erase memory (0x44) failed")

    def _encode_addr(self, addr):
        byte3 = (addr >> 0) & 0xFF
        byte2 = (addr >> 8) & 0xFF
        byte1 = (addr >> 16) & 0xFF
        byte0 = (addr >> 24) & 0xFF
        adrstruct = [byte0, byte1, byte2, byte3]
        return  adrstruct


    def cmdWriteMemory(self, addr, data):
        assert (len(data) <= 256)
        lng = (len(data) - 1) & 0xFF

        offs = 0
        length = 0

        command = self._encode_addr(addr)
        command.append(lng)
        if self.cmdGeneric(0x31, command):
            while offs < lng:
                partdata = data[offs:offs+8]
                self.cmdGeneric(0x04, partdata)
                offs +=8
            # print("\tOK")
            self._wait_for_ask("data transfered not properly")
        else:
            raise CmdException("Write memory (0x31) failed")

    def cmdReadMemory(self, addr, lng):
        assert(lng <= 256)
        N = (lng - 1) & 0xFF
        command = self._encode_addr(addr)
        command.append(N)
        readoutdata = bytearray()
        offs = 0
        if self.cmdGeneric(0x11, command):
            # for i in lng/8:
            while offs < lng:
                self.msg = self.bus.recv()
                readoutdata +=self.msg.data
                offs += 8
            self._wait_for_ask("data readout not properly")
            return readoutdata
        else:
            raise CmdException("ReadMemory (0x11) failed")

    def writeMemory(self, addr, data):
        print("*** Write command\n\r    |\n\r    ", end="")
        lng = len(data)
        offs = 0
        if usebar:
            writebar = tqdm(total=lng, desc="Writing:", unit= "b", unit_scale=True)
        while lng > 256:
            if usebar:
                writebar.update(256)
            else:
                print("    '->Write %(len)d bytes at 0x%(addr)X" % {'addr': addr, 'len': 256}, end="")
            self.cmdWriteMemory(addr, data[offs:offs + 256])
            offs += 256
            addr += 256
            lng -= 256
        if usebar:
            writebar.update(lng)
            writebar.close()
        else:
            print("    '->Write %(len)d bytes at 0x%(addr)X" % {'addr': addr, 'len': lng}, end="")
        self.cmdWriteMemory(addr, data[offs:offs + lng])

    def readMemory(self, addr, lng):
        print("*** Read command\n\r    |\n\r    ", end="")
        data = bytearray()
        if usebar:
            readbar = tqdm(total=lng, desc="Reading:", unit= "b", unit_scale=True)
        while lng > 256:
            if usebar:
                readbar.update(256)
            else:
                print("    '->Read %(len)d bytes at 0x%(addr)X" % {'addr': addr, 'len': 256})
            data += self.cmdReadMemory(addr, 256)
            addr += 256
            lng -= 256

        if usebar:
            readbar.update(lng)
            readbar.close()
        else:
            print("    '->Read %(len)d bytes at 0x%(addr)X" % {'addr': addr, 'len': lng})
        data = data + self.cmdReadMemory(addr, lng)
        return data



    def cmdWriteUnprotect(self):
        print("*** Write Unprotect command\n\r    |\n\r    ", end="")
        if self.cmdGeneric(0x73):
            self._wait_for_ask("0x73 write unprotect failed")
            # self._wait_for_ask("0x73 write unprotect 2 failed")
            print("'-> Write Unprotect done")
        else:
            raise CmdException("Write Unprotect (0x73) failed")




if __name__ == "__main__":

    # Unless verification of firmware successful, default exit code is 1
    exit_code = 1

    conf = {
        'port': 'can1',
        'baud': "1M",  # 125k, 250k, 500k, 1M
        'address': 0x08000000,
        'file': "stm32firmware.bin",
        'erase': 0,
        'write': 0,
        'verify': 0,
        'read': 0,
        'length': 0,
        'go_addr': -1,
    }

    parser = argparse.ArgumentParser()
    parser.add_argument("-e", "--erase", help="erase entire memory",action='store_true')
    parser.add_argument("-v", "--verify", help="verifies the memory from the uP and the file", action='store_true')
    parser.add_argument("-w", "--write", help="write the file to uP", action='store_true')
    parser.add_argument("-r", "--read", help="read the memory from the uP", action='store_true')
    parser.add_argument("-l", "--length", type=int, help="Length of read" )
    parser.add_argument("-p", "--port", help="can port id (default: can1)", default="can1")
    parser.add_argument("-b", "--baud", help="Baud speed of 125k, 250k, 500k, 1M (default=1M)", default="1M")
    parser.add_argument("-a", "--addr", type=hex, help="Target address" )
    parser.add_argument("-nrst", "--reset_gpio", type=int, help="Number of NRST gpio pin")
    parser.add_argument("-boot", "--boot_gpio", type=int, help="Number of BOOT gpio pin")
    parser.add_argument("file", help="File location")

    args = parser.parse_args()

    if args.erase:
        conf['erase'] = 1
    if args.write:
        conf['write'] = 1
    if args.read:
        conf['read'] = 1
    if args.verify:
        conf['verify'] = 1
    if args.length is not None:
        conf['length'] = args.length

    conf['port'] = args.port
    conf['baud'] = args.baud
    conf['file'] = args.file

    cmd = CommandInterface(conf['port'], boot=args.reset_gpio, nrst=args.boot_gpio)
    cmd.setCANspeed()

    cmd.open()

    try:
        try:
            if cmd.initChip() is 1:
                print("'->Manage to syncro with uP")

            # Change CAN speed on STM32 and BBB
            # cmd.cmdSpeed(port = conf['port'], speed=conf['baud'])

            # Write unprotect
            # cmd.cmdWriteUnprotect()

            # Get bootloader version
            cmd.cmdGet()

            # Get chip ID
            cmd.cmdGetID()

            if conf['write'] or conf['verify']:
                # Get binary file into array
                file = open(conf['file'], "rb")
                firmware = file.read()

            # Extended Erase
            if conf['erase']:
                cmd.cmdEraseMemory()

            if conf['write']:
                # Write the memory
                cmd.writeMemory(conf['address'], firmware)

            if conf['verify']:
                #verify the content
                new_firmware = cmd.readMemory(conf['address'], len(firmware))
                if firmware == new_firmware:
                    print("Verification OK")
                    exit_code = 0
                else:
                    print("Verification FAILED")

            if not conf['write'] and conf['read']:
                if conf['length'] is 0 or conf['length'] is None:
                    print("No number given to readout")
                else:
                    new_firmware = cmd.readMemory(conf['address'],conf['length'])
                    new_file = open(conf['file'], 'wb')
                    new_file.write(new_firmware)
                    new_file.close()
            pass
        except:
            print("Can't init. Ensure that BOOT0 is enabled and reset device")

    finally:
        cmd.bus.shutdown()
        # cmd.releaseChip()
        pass
        sys.exit(exit_code)
